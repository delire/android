package fr.asclepios.sirhp.allergy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.utils.CheckableLinearLayout;
import fr.asclepios.sirhp.entity.Allergy;

public class AllergyListAdapter extends BaseAdapter
{
    List<Allergy>	allergies;
    LayoutInflater	inflater;
    List<Allergy> checked = new ArrayList<>();

    public AllergyListAdapter (Context context, List<Allergy> allergies)
    {
        inflater = LayoutInflater.from (context);
        this.allergies = allergies;
    }

    @Override
    public int getCount ()
    {
        return allergies.size ();
    }

    @Override
    public Object getItem (int position)
    {
        return allergies.get (position);
    }

    @Override
    public long getItemId (int position)
    {
        return position;
    }

    public  List<Allergy> getChecked(){
        return checked;
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null)
        {
            holder = new ViewHolder ();
            convertView = inflater.inflate (R.layout.add_allergy_name_checkbox, null);
            holder.name = (TextView) convertView.findViewById (R.id.addAllergy_name);
            holder.id = (TextView) convertView.findViewById(R.id.addAllergy_id);
            holder.checked = (CheckableLinearLayout)convertView;
        }
        else
        {
            holder = (ViewHolder) convertView.getTag ();
        }
        holder.id.setText(allergies.get(position).getIdAllergy().toString());
        holder.name.setText(allergies.get(position).getAllergyName ());
        //holder.position = allergies.get(position).getIdAllergy();
        convertView.setOnClickListener(v -> {
            CheckableLinearLayout c = (CheckableLinearLayout)v;
            ViewHolder vh = ((ViewHolder) c.getTag());
            if (c.isChecked()) {
                //c.setChecked(true);
                checked.add(allergies.get(position));
                allergies.get(position).setChecked(true);
            } else {
                //c.setChecked(false);
                checked.remove(allergies.get(position));
                allergies.get(position).setChecked(false);
            }

        });
        holder.checked.setChecked(allergies.get(position).isChecked());
        convertView.setTag (holder);
        return convertView;
    }



    //Classe permettant de sauvegarder l'etat de la personne et de pouvoir recuperer la position.
    public class ViewHolder
    {
        TextView            name;
        TextView	        id;
        CheckableLinearLayout checked;
        int                 position;
    }
}