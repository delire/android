package fr.asclepios.sirhp.settings;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.util.Log;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.entity.Location;
import fr.asclepios.sirhp.location.LocationService;


public class SettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener, ServiceConnection {
    private static final String LOG_TAG = SettingsFragment.class.getSimpleName();
    private LocationService locationService;
    private boolean bound = false;
    private Context context;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
        final SwitchPreferenceCompat locationSwitch = (SwitchPreferenceCompat) findPreference("location_switch");
        locationSwitch.setOnPreferenceChangeListener(this);
        context = getContext();
        Intent intent = new Intent(context, LocationService.class);
        context.bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Boolean checked = (Boolean) newValue;
        if (checked) {
            Log.d(LOG_TAG, "location switch on");
            if(bound)
                locationService.start(true);
            else
                Log.e(LOG_TAG, "location service not bound");
        } else {
            Log.d(LOG_TAG, "location switch off");
            if(bound)
                locationService.stop();
            else
                Log.e(LOG_TAG, "location service not bound");
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        context.unbindService(this);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        LocationService.MyBinder binder = (LocationService.MyBinder) service;
        locationService = binder.getService();
        bound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        bound = false;
    }
}
