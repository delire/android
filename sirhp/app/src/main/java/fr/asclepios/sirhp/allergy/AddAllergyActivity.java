package fr.asclepios.sirhp.allergy;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.entity.Allergy;
import fr.asclepios.sirhp.request.CustomJsonArrayRequest;
import fr.asclepios.sirhp.request.CustomJsonObjectRequestArrayResponse;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class AddAllergyActivity extends ListActivity {
    private static final String LOG_TAG = AddAllergyActivity.class.getSimpleName();

    private ListView list;
    private Button addAllergyBtn;
    private Button validateBtn;
    private Integer dmpId;
    private String token;
    private String baseUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_add_allergy);

        addAllergyBtn = findViewById(R.id.addAllergy_add);
        validateBtn = findViewById(R.id.addAllergy_validate);
        token = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE).getString("token", "");
        baseUrl = getSharedPreferences("GLOBALS", MODE_PRIVATE).getString("url", null);

        Bundle b = getIntent().getExtras();
        dmpId = -1; // or other values
        if(b != null) {
            dmpId = b.getInt("id");
        }

        addAllergyBtn.setOnClickListener((View w) -> {
            Intent i = new Intent(this, NewAllergyActivity.class);
            startActivityForResult(i, 1);
        });

        validateBtn.setOnClickListener((View w) -> {
            sendAddAllergy(((AllergyListAdapter) list.getAdapter()).getChecked());
        });
        JSONObject request = new JSONObject();
        sendGetLoadRequest(request, baseUrl + "allergy/getAllAllergies/");

    }

    @Override
    protected void onResume(){
        JSONObject request = new JSONObject();
        sendGetLoadRequest(request, baseUrl + "allergy/getAllAllergies/");
        super.onResume();
    }

    private void sendAddAllergy(List<Allergy> allergies) {
        String url = baseUrl + "allergy/addMulipleAllergyPatient";
        JSONArray request =  new JSONArray();
        JSONObject newObj;
        for (Allergy allergy : allergies) {
            newObj = new JSONObject();
            JSONObject id = new JSONObject();
            try {
                id.put("idAllergy", allergy.getIdAllergy());
                id.put("idDmp", dmpId);
                newObj.put("id", id );
                request.put(newObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        CustomJsonArrayRequest jsonRequest = new CustomJsonArrayRequest
                (Request.Method.POST, url, request, token, response -> {
                    Intent i = new Intent();
                    ToasterMaker.makeToast(this,"Allergy added");
                    setResult(RESULT_OK, i);
                    finish();
                }, error -> {
                    Intent i = new Intent();
                    ToasterMaker.makeToast(this,"Error happened");
                    Log.e(LOG_TAG, error.toString());
                    setResult(2, i);
                    finish();
                });
        HttpSender.getInstance(this).addToRequestQueue(jsonRequest);
    }

    private void sendGetLoadRequest(JSONObject request, String url) {
        CustomJsonObjectRequestArrayResponse jsonObjectRequest = new CustomJsonObjectRequestArrayResponse
                (Request.Method.GET, url, request, token, this::fillListView, error -> {
                    ToasterMaker.makeToast(this, "Add allergy to patient Error");
                    Log.e(LOG_TAG, error.toString());
                });
        HttpSender.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }


    private void fillListView(JSONArray response) {

        list = findViewById(android.R.id.list);
        List<Allergy> allergies = new ArrayList<>();
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject allergyJson = response.getJSONObject(i);
                Allergy allergy = new Allergy();
                allergy.setAllergyName(allergyJson.getString("allergyName"));
                allergy.setIdAllergy(allergyJson.getInt("idAllergy"));
                allergy.setChecked(false);
                allergies.add(allergy);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        AllergyListAdapter mSchedule = new AllergyListAdapter(this.getBaseContext(), allergies);
        list.setAdapter(mSchedule);

    }
}






