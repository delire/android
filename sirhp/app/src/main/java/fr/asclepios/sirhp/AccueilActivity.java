package fr.asclepios.sirhp;

import android.content.SharedPreferences;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;



import android.util.Log;
import android.view.View;
import android.widget.TextView;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import fr.asclepios.sirhp.acte.ActeActivity;
import fr.asclepios.sirhp.acte.ShowActeActivity;
import fr.asclepios.sirhp.dmp.FindDmpFragment;
import fr.asclepios.sirhp.dmp.NewDmpFragment;
import fr.asclepios.sirhp.document.DocumentActivity;
import fr.asclepios.sirhp.user.CreateUserFragment;
import fr.asclepios.sirhp.user.ListUserFragment;
import fr.asclepios.sirhp.user.ProfilUserFragment;

import fr.asclepios.sirhp.utils.MessageNotificationService;

public class AccueilActivity extends AppCompatActivity {
    private static final String LOG_TAG = AccueilActivity.class.getSimpleName();
    public static final String CHANNEL_ID = "1234";
    private TextView val;
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "NotificationChannel", importance);
            channel.setDescription("Channel of Notification app");
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager == null) return ;
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);


        createNotificationChannel();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        SharedPreferences.Editor editor = sp.edit();
        editor.clear();

        Intent serviceNotification = new Intent(this, MessageNotificationService.class);
        editor.putString("url","ws://192.168.56.1:1818/notifications/android");
        editor.putBoolean("active", true);

        editor.apply();
        serviceNotification.setAction(MessageNotificationService.START_WATCH_ACTION);
        startService(serviceNotification);
    }



    public void launchNewUser(View view)
    {
        Intent i = new Intent(this,CreateUserFragment.class);
        startActivityForResult(i,1);
    }

    public void launchProfilUser(View view)
    {
        Intent i = new Intent(this,ProfilUserFragment.class);
        startActivityForResult(i,1);
    }

    public void launchListUsers(View view)
    {
        Intent i = new Intent(this,ListUserFragment.class);
        startActivityForResult(i,1);
    }



    public void launchActe(View view) {
        Log.d(LOG_TAG, "Loading Acte");
        Intent myIntent = new Intent(this, ActeActivity.class);
        startActivity(myIntent);
    }

    public void launchNewDmp(View view) {
        Log.d(LOG_TAG, "Loading New Dmp");
        Intent i = new Intent(this,NewDmpFragment.class);
        startActivityForResult(i,1);
    }

    public void launchFindDmp(View view) {
        Log.d(LOG_TAG, "Loading Find Dmp");
        Intent i = new Intent(this,FindDmpFragment.class);
        startActivityForResult(i,1);
    }

}

