package fr.asclepios.sirhp.location;

import android.location.Location;

interface LocationObserver {
    void notify(Location location);
}
