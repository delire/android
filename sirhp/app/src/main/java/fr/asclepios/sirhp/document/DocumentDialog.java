package fr.asclepios.sirhp.document;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.entity.Document;

public class DocumentDialog extends Dialog {

    private DocumentActivity context;
    private Document document;
    private DateFormat dateFormat;

    private TextView filenameTextView;
    private TextView dateCreationTextView;
    private TextView dateModificationTextView;
    private ImageButton downloadBtn;
    private ImageButton updateBtn;
    private ImageButton validateBtn;
    private ImageButton deleteBtn;

    public DocumentDialog(DocumentActivity context, Document document) {
        super(context);
        this.context = context;
        this.document = document;
        this.dateFormat = DateFormat.getDateTimeInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_document);

        filenameTextView = (TextView) findViewById(R.id.filenameTextView);
        dateCreationTextView = (TextView) findViewById(R.id.dateCreationTextView);
        dateModificationTextView = (TextView) findViewById(R.id.dateModificationTextView);
        downloadBtn = (ImageButton) findViewById(R.id.downloadBtn);
        updateBtn = (ImageButton) findViewById(R.id.updateBtn);
        validateBtn = (ImageButton) findViewById(R.id.validateBtn);
        deleteBtn = (ImageButton) findViewById(R.id.deleteBtn);

        downloadBtn.setOnClickListener(context);
        updateBtn.setOnClickListener(context);
        validateBtn.setOnClickListener(context);
        deleteBtn.setOnClickListener(context);

        show(document);
    }

    public void show(Document document) {
        filenameTextView.setText(document.getFilename());
        dateCreationTextView.setText(context.getResources().getString(R.string.doc_created) + " : " + dateFormat.format(document.getDateCreation()));
        Date dateModification = document.getDateModification();
        if (dateModification == null)
            dateModificationTextView.setVisibility(View.INVISIBLE);
        else {
            dateModificationTextView.setVisibility(View.VISIBLE);
            dateModificationTextView.setText(context.getResources().getString(R.string.doc_modified) + " : " + dateFormat.format(document.getDateModification()));
        }

        if (document.getValidation()) {
            updateBtn.setVisibility(View.INVISIBLE);
            validateBtn.setVisibility(View.INVISIBLE);
            deleteBtn.setVisibility(View.INVISIBLE);
        }
    }
}
