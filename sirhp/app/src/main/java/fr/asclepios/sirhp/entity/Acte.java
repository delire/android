package fr.asclepios.sirhp.entity;

public class Acte {
    private boolean validationActe;
    private int idActe;
    public Acte(int idActe, boolean validationActe){
        this.idActe = idActe;
        this.validationActe = validationActe;
    }

    public boolean isValidationActe() {
        return validationActe;
    }

    public void setValidationActe(boolean validationActe) {
        this.validationActe = validationActe;
    }

    public int getIdActe() {
        return idActe;
    }

    public void setIdActe(int idActe) {
        this.idActe = idActe;
    }
}
