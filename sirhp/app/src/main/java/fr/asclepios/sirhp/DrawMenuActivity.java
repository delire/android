package fr.asclepios.sirhp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import fr.asclepios.sirhp.connexion.LoginActivity;
import fr.asclepios.sirhp.dmp.FindDmpFragment;
import fr.asclepios.sirhp.dmp.NewDmpFragment;
import fr.asclepios.sirhp.location.LocationService;
import fr.asclepios.sirhp.user.CreateUserFragment;
import fr.asclepios.sirhp.user.ListUserFragment;
import fr.asclepios.sirhp.user.ProfilUserFragment;
import fr.asclepios.sirhp.user.SelfProfilFragment;

public class DrawMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ServiceConnection {
    private static final String LOG_TAG = DrawMenuActivity.class.getSimpleName();
    private static final String URL = "http://10.0.2.2:8080/";
    private static final int LOGIN_REQUEST = 1;
    private static final String mainFragmentName = "mainFragment";

    private TextView fullname;
    private TextView email;

    private boolean bound;

    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE);
        userId = pref.getString("id","");

        View navHeaderView = navigationView.getHeaderView(0);
        ((TextView)navHeaderView.findViewById(R.id.acccueilFirstNameLastName)).setText(pref.getString("fullname",""));
        ((TextView)navHeaderView.findViewById(R.id.acccueilEmail)).setText(pref.getString("email", ""));

        MainFragment mainFragment = new MainFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container, mainFragment);
        transaction.addToBackStack(null);
        transaction.commit();

        this.getSharedPreferences("GLOBALS", Context.MODE_PRIVATE).edit()
                .putString("url", URL)
                .apply();

        launchLogin();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "starting location service");
        bound = false;
        Intent intent = new Intent(this, LocationService.class);
        bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profil) {
            SelfProfilFragment selfProfilFragment = new SelfProfilFragment();
            Bundle args = new Bundle();
            args.putInt(FindDmpFragment.ARG_POSITION, item.getItemId());
            args.putString("id",userId);
            launchFragment(selfProfilFragment,args);

        } else if (id == R.id.nav_dmp) {

            FindDmpFragment findDmpFragment = new FindDmpFragment();
            Bundle args = new Bundle();
            args.putInt(FindDmpFragment.ARG_POSITION, item.getItemId());
            launchFragment(findDmpFragment, args);

        } else if (id == R.id.nav_dmp_create) {
            NewDmpFragment newDmpFragment = new NewDmpFragment();
            Bundle args = new Bundle();
            args.putInt(NewDmpFragment.ARG_POSITION, item.getItemId());
            launchFragment(newDmpFragment, args);

        } else if (id == R.id.create_user) {
            CreateUserFragment createUserFragment = new CreateUserFragment();
            Bundle args = new Bundle();
            args.putInt(NewDmpFragment.ARG_POSITION, item.getItemId());
            launchFragment(createUserFragment, args);

        } else if (id == R.id.nav_logout) {
            Intent i = new Intent(this, LoginActivity.class);
            // set the new task and clear flags
            SharedPreferences pref = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            editor.remove("token");
            editor.commit();
            startActivity(i);
        } else if (id == R.id.nav_main) {
            startActivity(new Intent(this, AccueilActivity.class));
        }
        else if (id == R.id.nav_listUser){
            ListUserFragment listUserFragment = new ListUserFragment();
            Bundle args = new Bundle();
            args.putInt(NewDmpFragment.ARG_POSITION, item.getItemId());
            launchFragment(listUserFragment, args);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void launchFragment(Fragment fragment, Bundle bundle){
        getSupportFragmentManager().popBackStack(mainFragmentName,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(mainFragmentName);
        transaction.commit();
    }

    private void launchLogin()
    {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, LOGIN_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST) {
            Log.d(LOG_TAG, "Logged in");
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        bound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        bound = false;
    }
}
