package fr.asclepios.sirhp.user;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.request.CustomJsonArrayRequest;
import fr.asclepios.sirhp.request.HttpSender;

public class ListUserFragment extends Fragment implements AdapterView.OnItemClickListener {
    private String token;
    private List<HashMap<String, String>> liste = new ArrayList<HashMap<String, String>>();
    private ListView listview;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.activity_list_user, container, false);
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        String urlServer = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);


        listview = v.findViewById(R.id.listView1);
        listview.setOnItemClickListener(this);

        getAllUser(urlServer);

        return v;
    }
    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        Log.i("HelloListView", "You clicked Item: " + id + " at position:" + position);
        // Then you start a new Activity via Intent
        HashMap<String, String> liste =  (HashMap<String, String>) l.getItemAtPosition(position);


        ProfilUserFragment profilUserFragment = new ProfilUserFragment();
        Bundle b = new Bundle();
        b.putString("id", liste.get("id"));
        profilUserFragment.setArguments(b);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, profilUserFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public void getAllUser(String urlServer){
        String url = urlServer+"printAllUser";
        CustomJsonArrayRequest jsonArrayRequest = new CustomJsonArrayRequest
                (Request.Method.GET, url, null, token, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try{
                            // Loop through the array elements
                            for(int i=0;i<response.length();i++){
                                HashMap<String,String> l  = new HashMap<String,String>();

                                JSONObject student = response.getJSONObject(i);

                                String idUser = student.getString("id");
                                String email = student.getString("email");
                                String lastName = student.getString("lastName");
                                String firstName = student.getString("firstName");
                                l.put("id",idUser);
                                l.put("email",email);
                                l.put("lastName",lastName);
                                l.put("firstName",firstName);

                                liste.add(l);
                            }

                            ListAdapter adapter = new SimpleAdapter(getContext(),
                                    liste, android.R.layout.simple_list_item_activated_2,
                                    new String[] {"id","email","lastName", "fistName"},
                                    new int[] {android.R.id.text1, android.R.id.text2 });

                            listview.setAdapter(adapter);

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });



        HttpSender.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);

    }

}
