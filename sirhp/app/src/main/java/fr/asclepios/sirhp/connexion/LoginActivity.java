
package fr.asclepios.sirhp.connexion;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import fr.asclepios.sirhp.MainActivity;
import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.ToasterMaker;


public class LoginActivity extends AppCompatActivity {
    private static final String LOG_TAG = LoginActivity.class.getSimpleName();

    private EditText email;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (EditText)findViewById(R.id.etName);
        password = (EditText)findViewById(R.id.etPassword);
        String token = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE).getString("token", "");
        if (!token.isEmpty()){
            setResult(RESULT_OK);
            finish();
        }
        HttpSender sender = new HttpSender(this);
        auth("admin@sirhp.com", "adminpwd");//TODO : remove
    }

    private void auth(String userName, String userPassword){
        JSONObject request = new JSONObject();
        try {
            request.put("login", userName);
            request.put("password", userPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = getSharedPreferences("GLOBALS", MODE_PRIVATE).getString("url", null);
        sendPostRequest(request,  url + "auth");
    }
    private void sendPostRequest(JSONObject request, String url) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, request, this::fillToken, error -> {
                    ToasterMaker.makeToast(this, "Connection Error");
                    Log.e(LOG_TAG, error.toString());
                });

        // Access the RequestQueue through your singleton class.
        HttpSender.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }
    private void fillToken(JSONObject response){
        try {
            String token = (String) response.get("token");
            JSONObject user = (JSONObject)  response.get("user");
            String fname = (String) user.get("firstName");
            String lname = (String) user.get("lastName");
            String email = (String) user.get("email");
            String id = user.getString("id");

            Log.d(LOG_TAG, token);
            Log.d(LOG_TAG, "id user = " + id);
            SharedPreferences pref = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            editor.putString("token", token);
            editor.putString("fullname", fname + " " + lname);
            editor.putString("email", email);
            editor.putString("id", id);

            editor.commit();
            setResult(RESULT_OK);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void login(View view)
    {
        //TODO : process empty fields
        auth(email.getText().toString(), password.getText().toString());
    }
}

