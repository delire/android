package fr.asclepios.sirhp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import fr.asclepios.sirhp.connexion.LoginActivity;

public class MainActivity extends AppCompatActivity {


    static public SharedPreferences sharedPreferences;

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final String URL = "http://10.0.2.2:8080/";
    private static final int LOGIN_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getSharedPreferences("GLOBALS", Context.MODE_PRIVATE).edit()
                .putString("url", URL)
                .apply();
        launchLogin();
    }

    private void launchLogin()
    {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, LOGIN_REQUEST);
    }

    private void launchWelcome()
    {
        Log.d(LOG_TAG, "Logged in");
        Intent intent = new Intent(this, AccueilActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST) {
            if (resultCode == RESULT_OK) {
                launchWelcome();
            }
        }
    }
}
