package fr.asclepios.sirhp.entity.enumeration;

public enum DocumentType {
    TEXT, IMAGE, VIDEO
}
