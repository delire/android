package fr.asclepios.sirhp.user;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import fr.asclepios.sirhp.request.CustomJsonArrayRequest;
import fr.asclepios.sirhp.request.CustomJsonObjectRequest;
import fr.asclepios.sirhp.request.CustomStringRequest;
import fr.asclepios.sirhp.request.HttpSender;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.utils.MultiSelectionSpinner;
import fr.asclepios.sirhp.utils.ToasterMaker;

import static java.lang.Integer.parseInt;

public class ProfilUserFragment extends Fragment {
    private String token;
    private EditText dateEdit;
    private DatePickerDialog datePickerDialog;
    private EditText firstName;
    private EditText lastName;
    private  EditText street;
    private EditText city;
    private EditText postalCode;
    private String id;
    private EditText country;
    private EditText phone1;
    private EditText phone2;
    private EditText email;
    private EditText password;
    private RadioGroup radioGroup;
    private RadioButton radioButton;

    private RadioGroup genderRadioGroup;
    private RadioButton femaleRadio;
    private RadioButton maleRadio;

    private MultiSelectionSpinner spinnerRole;
    private ArrayList<String> rolesNames = new ArrayList<String>();
    private ArrayList<LinkedHashMap<String,String>> roles  = new ArrayList<LinkedHashMap<String,String>>();
    private HashMap<Integer, String> positionToRole  = new HashMap<Integer,String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.activity_profil_user, container, false);
        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        String urlServer = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);
        id = (String) getArguments().get("id");

        firstName = v.findViewById(R.id.first_name_edit);
        lastName = v.findViewById(R.id.last_name_edit);
        street = v.findViewById(R.id.rue_edit);
        country = v.findViewById(R.id.country_edit);
        city = v.findViewById(R.id.ville_edit);
        postalCode = v.findViewById(R.id.code_postal_edit);
        phone1 = v.findViewById(R.id.phone1_edit);
        phone2 = v.findViewById(R.id.phone2_edit);
        email = v.findViewById(R.id.email_edit);
        password = v.findViewById(R.id.password_edit);
        maleRadio = v.findViewById(R.id.maleRadioButton);
        femaleRadio = v.findViewById(R.id.maleRadioButton);

        spinnerRole = (MultiSelectionSpinner) v.findViewById(R.id.spinnerRole);

        Button btnUpdateUser = v.findViewById(R.id.button_update);
        Button btnDeleteUser = v.findViewById(R.id.button_delete);
        dateEdit =  v.findViewById(R.id.profil_birthday_edit);
        // perform click event on edit text

        getAllRole(urlServer);



        btnUpdateUser.setOnClickListener((View w) -> {

            JSONObject object = new JSONObject();
            JSONArray arrayChoice = new JSONArray();

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
//"yyyy-MM-dd HH:mm:ss"
            // 192.168.56.1
            firstName = v.findViewById(R.id.first_name_edit);
            lastName = v.findViewById(R.id.last_name_edit);
            street = v.findViewById(R.id.rue_edit);
            country = v.findViewById(R.id.country_edit);
            city = v.findViewById(R.id.ville_edit);
            postalCode = v.findViewById(R.id.code_postal_edit);
            phone1 = v.findViewById(R.id.phone1_edit);
            phone2 = v.findViewById(R.id.phone2_edit);
            email = v.findViewById(R.id.email_edit);
            password = v.findViewById(R.id.password_edit);
            radioGroup = (RadioGroup) v.findViewById(R.id.genderRadioGroup);
            // get selected radio button from radioGroup
            int selectedId = radioGroup.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            radioButton = (RadioButton) v.findViewById(selectedId);
            try {

                try{
                    Date localDate = formatter.parse(dateEdit.getText().toString());
                    object.put("birthday", formatDate.format(localDate));

                }catch(ParseException e){
                    object.put("birthday", dateEdit.getText().toString());

                }

                object.put("firstName",firstName.getText().toString());
                object.put("id",id);
                object.put("lastName", lastName.getText().toString());
                object.put("sexe", radioButton.getText().toString());
                object.put("postalCode", postalCode.getText().toString());
                object.put("phonePrimary", phone1.getText().toString());
                object.put("phoneSecondary", phone2.getText().toString());
                object.put("city", city.getText().toString());
                object.put("country", country.getText().toString());
                object.put("email", email.getText().toString());
                object.put("street", street.getText().toString());
                object.put("password", password.getText().toString());
                List<String> roleChoice = spinnerRole.getSelectedStrings();
                List<Integer> indeiceChoice = spinnerRole.getSelectedIndicies();
                Log.i("roleChoice" , " messa : " + spinnerRole.getSelectedStrings());
                Log.i("getSelectedIndicies" , " messa : " + spinnerRole.getSelectedIndicies());
                Log.i("positionToRole" , " messa : " + positionToRole);
                for(int i = 0; i< roleChoice.size(); i++){
                    JSONObject roleC = new JSONObject();
                    roleC.put("roleId", positionToRole.get(indeiceChoice.get(i)));
                    roleC.put("roleName", roleChoice.get(i));
                    arrayChoice.put(roleC);

                }
                Log.i("arrayChoice","message : " + arrayChoice);

                object.put("addedIdRole", arrayChoice);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // a potentially time consuming task
            sendPostRequest(object, urlServer+"updateUser");
        });

        btnDeleteUser.setOnClickListener((View w) -> {
            String url = urlServer+"deleteUser/"+id;
            Log.i("DeleteUser", "delete user : " + url );

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Display the first 500 characters of the response string.
                            try {
                                JSONObject json= (JSONObject) new JSONTokener(response).nextValue();
                                getActivity().onBackPressed();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            HttpSender.getInstance(getContext()).addToRequestQueue(stringRequest);
        });
        dateEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                StringBuilder sb = new StringBuilder();
                sb.append( Calendar.YEAR);
                c.set(c.get(Calendar.YEAR) - 18, c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                dateEdit.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        return v;
    }

    private void sendPostRequest(JSONObject request, String url){
        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest
                (Request.Method.POST, url, request, token, response -> {
                    Intent i = new Intent();
                    ToasterMaker.makeToast(getContext(),"Utilisateur mis a jour");
                    getActivity().onBackPressed();
                }, error -> {
                    Intent i = new Intent();
                    ToasterMaker.makeToast(getContext(),"fail");
                    getActivity().onBackPressed();
                });

        // Access the RequestQueue through your singleton class.
        HttpSender.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void getAllRole(String url){
        CustomJsonArrayRequest jsonArrayRequest = new CustomJsonArrayRequest
                (Request.Method.GET, url+"getAllRoles", null, token, response -> {
                    try{
                        // Loop through the array elements
                        List<String> sL = new ArrayList<String>();
                        for(int i=0;i<response.length();i++){
                            LinkedHashMap l  = new LinkedHashMap<>();

                            // Get current json object
                            JSONObject student = response.getJSONObject(i);

                            // Get the current student (json object) data
                            String roleId = student.getString("roleId");
                            String roleName = student.getString("roleName");
                            l.put("roleId",roleId);
                            l.put("roleName",roleName);
                            sL.add(roleName);
                            rolesNames.add(roleName);
                            roles.add(l);
                            positionToRole.put(i,roleId);
                        }
                        spinnerRole.setItems(sL);
                        getUser(url+"printUser/"+id);
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }, error -> {
                });
        HttpSender.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);
    }

    private void getUser(String url){
        // Request a string response from the provided URL.
        CustomStringRequest stringRequest = new CustomStringRequest(Request.Method.GET, url, token,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONObject json= (JSONObject) new JSONTokener(response).nextValue();
                            JSONArray json2 = json.getJSONArray("gotRole");
                            ArrayList<Integer> rabJ = new ArrayList<>();
                            for(int i = 0 ; i < json2.length(); i++){
                                JSONObject tmpSon = json2.getJSONObject(i);
                                for(int j = 0; j < roles.size(); j++){
                                    if(parseInt(roles.get(j).get("roleId")) == parseInt(tmpSon.get("role").toString())){
                                        rabJ.add(j);
                                    }
                                }
                            }
                            spinnerRole.setSelection(rabJ);
                            firstName.setText(json.get("firstName").toString());
                            lastName.setText(json.get("lastName").toString());
                            street.setText(json.get("street").toString());
                            city.setText(json.get("city").toString());
                            postalCode.setText(json.get("postalCode").toString());
                            country.setText(json.get("country").toString());
                            phone1.setText(json.get("phonePrimary").toString());
                            phone2.setText(json.get("phoneSecondary").toString());
                            email.setText(json.get("email").toString());
                            password.setText(json.get("password").toString());
                            dateEdit.setText(json.get("birthday").toString());

                            if(json.get("sexe").toString().toLowerCase().equals("homme")){

                                maleRadio.setChecked(true);
                            }else{
                                femaleRadio.setChecked(true);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        HttpSender.getInstance(getContext()).addToRequestQueue(stringRequest);
    }
}
