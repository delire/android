package fr.asclepios.sirhp.location;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.android.core.permissions.PermissionsManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import fr.asclepios.sirhp.request.CustomJsonObjectRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.settings.SettingsActivity;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class LocationService extends Service {
    private static final String LOG_TAG = LocationService.class.getSimpleName();
    public static final String URL_PATH = "locations/";
    public static final String ACTION_LOCATION_PERMISSION = "fr.asclepios.sirhp.LOCATION_PERMISSION_REQUEST";
    public static final String LOCATION_PERMISSION = "fr.asclepios.sirhp.extra.LOCATION_PERMISSION";
    private static final long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;// 1 second
    private static final long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;

    private String url;
    private LocationEngine locationEngine;
    private LocationService.LocationCallback callback;
    private LocationEngineRequest request;
    private final IBinder mBinder = new MyBinder();
    private boolean handlerBound;
    private boolean enabled;
    private LocationObserver locationObserver;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");

        String baseUrl = getSharedPreferences("GLOBALS", MODE_PRIVATE).getString("url", null);
        url = baseUrl + URL_PATH;

        callback = new LocationService.LocationCallback(this);
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);
        request = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();
        handlerBound = false;
        locationObserver = null;
        enabled = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.KEY_PREF_LOCATION_SWITCH, false);
        start(enabled);
    }

    @SuppressLint("MissingPermission")
    public void start(boolean enabled) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            if(enabled && !handlerBound)
            {
                Log.d(LOG_TAG, "location monitoring enabled");
                locationEngine.requestLocationUpdates(request, callback, getMainLooper());
                locationEngine.getLastLocation(callback);
                handlerBound = true;
            }
            if (!enabled)
                Log.d(LOG_TAG, "location disabled");
        } else {
            Log.d(LOG_TAG, "requesting location permission");
            Intent intent = new Intent(getBaseContext(), LocationPermissionActivity.class);
            startActivity(intent);
        }
    }

    public void stop()
    {
        Log.d(LOG_TAG, "location monitoring disabled");
        locationEngine.removeLocationUpdates(callback);
        handlerBound = false;
    }

    public void subscribe(LocationObserver observer)
    {
        Log.d(LOG_TAG, "observer subscribed");
        locationObserver = observer;
    }

    public void free(LocationObserver observer)
    {
        if(locationObserver == observer)
            locationObserver = null;
        Log.e(LOG_TAG, "observer freeed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        if (intent != null) {
            final String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case ACTION_LOCATION_PERMISSION:
                        final boolean granted = intent.getBooleanExtra(LOCATION_PERMISSION, false);
                        if(granted)
                            start(enabled);
                       break;
                }
            }
        }
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return mBinder;
    }

    public class MyBinder extends Binder {
        public LocationService getService() {
            return LocationService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates(callback);
        }
    }

    private static class LocationCallback
            implements LocationEngineCallback<LocationEngineResult> {
        private final WeakReference<LocationService> serviceWeakReference;


        LocationCallback(LocationService service) {
            this.serviceWeakReference = new WeakReference<LocationService>(service);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            LocationService service = serviceWeakReference.get();

            if (service != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                Log.d(LOG_TAG, "location : " + location);
                if(service.locationObserver != null)
                    service.locationObserver.notify(location);

                JSONObject locationJson = new JSONObject();
                try {
                    locationJson.put("latitude", location.getLatitude());
                    locationJson.put("longitude", location.getLongitude());
                    locationJson.put("time", location.getTime());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String token = service.getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE).getString("token", "");
                CustomJsonObjectRequest loationRequest = new CustomJsonObjectRequest(Request.Method.POST, service.url, locationJson, token, response -> {
                    Log.d(LOG_TAG, "location sent to server");
                }, error -> Log.e(LOG_TAG, error.toString()));
                HttpSender.getInstance(service).addToRequestQueue(loationRequest);
            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.e(LOG_TAG, exception.getLocalizedMessage());
            LocationService service = serviceWeakReference.get();
            if (service != null) {
                ToasterMaker.makeToast(service, exception.getLocalizedMessage());
            }
        }
    }
}
