package fr.asclepios.sirhp.entity;

public class Allergy {

    public Integer idAllergy;
    public String allergyName;
    public boolean checked;

    public Integer getIdAllergy(){
        return idAllergy;
    }

    public String getAllergyName(){
        return allergyName;
    }

    public boolean isChecked(){
        return checked;
    }

    public void setIdAllergy(Integer idAllergy) {
        this.idAllergy = idAllergy;
    }

    public void setAllergyName(String allergyName) {
        this.allergyName = allergyName;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
