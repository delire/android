package fr.asclepios.sirhp.dmp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.request.CustomJsonObjectRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.DateMask;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class NewDmpFragment extends Fragment {
    private static final String LOG_TAG = NewDmpFragment.class.getSimpleName();

    public static String ARG_POSITION = "newDmpFrag_pos";

    private TextView firstName;
    private TextView lastName;
    private TextView securityNumber;
    private TextView dateBirth;
    private TextView phoneNumber;
    private TextView phoneNumberSec;
    private TextView street;
    private TextView postal;
    private TextView city;
    private TextView mail;
    private Switch organDonator;
    private TextView country;
    private RadioGroup dgSexe;

    private RadioButton sexeBtn;

    private RadioGroup dgBloodChar;
    private RadioButton rbBloodChar;

    private RadioGroup dgBloodRes;
    private RadioButton rbBloodRes;

    private String token;
    private String baseUrl;
    private final Calendar myCalendar = Calendar.getInstance();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.activity_new_dmp, container, false);

        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        baseUrl = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);

        firstName = v.findViewById(R.id.newDMP_firstNameET);
        lastName = v.findViewById(R.id.newDMP_lastNameET);
        securityNumber = v.findViewById(R.id.newDMP_securityNumberET);
        dateBirth = v.findViewById(R.id.newDMP_birthdayDATE);
        phoneNumber = v.findViewById(R.id.newDMP_phoneNumberET);
        phoneNumberSec = v.findViewById(R.id.newDMP_phoneNumberSecET);
        street = v.findViewById(R.id.newDMP_streetET);
        postal = v.findViewById(R.id.newDMP_postalET);
        city = v.findViewById(R.id.newDMP_cityET);
        mail = v.findViewById(R.id.newDMP_mailET);
        organDonator = v.findViewById(R.id.newDmp_donatorSwitch);
        country = v.findViewById(R.id.newDMP_countryET);

        dateBirth.addTextChangedListener(new DateMask());


        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };

        dateBirth.setOnClickListener(w -> {
            new DatePickerDialog(getActivity(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        Button btnValidate = v.findViewById(R.id.newDMP_btnFormDmpValidate);
        btnValidate.setOnClickListener((View w) -> {
            JSONObject object = new JSONObject();
            String url = baseUrl + "dmp/addDmp";
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);

            dgSexe = v.findViewById(R.id.newDMP_rgSexe);
            sexeBtn = v.findViewById(dgSexe.getCheckedRadioButtonId());

            dgBloodChar = v.findViewById(R.id.newDMP_rgBloodChar);
            rbBloodChar = v.findViewById(dgBloodChar.getCheckedRadioButtonId());

            dgBloodRes = v.findViewById(R.id.newDMP_rgBloodRes);
            rbBloodRes = v.findViewById(dgBloodRes.getCheckedRadioButtonId());

            try {

                object.put("firstName",firstName.getText().toString());
                object.put("lastName", lastName.getText().toString());
                object.put("securityNumber", securityNumber.getText().toString());
                if (dateBirth != null && !dateBirth.getText().toString().equals("")) {
                    Date dateStr = formatter.parse(dateBirth.getText().toString());
                    ((SimpleDateFormat) formatter).applyPattern("dd/MM/yyyy");
                    object.put("dateOfBirth", formatter.format(dateStr));
                }
                object.put("phoneNumberPatient", phoneNumber.getText().toString());
                object.put("phoneNumberSecPatient", phoneNumberSec.getText().toString());
                object.put("street", street.getText().toString());
                object.put("postal", postal.getText().toString());
                object.put("city", city.getText().toString());
                object.put("mailPatient", mail.getText().toString());
                object.put("organDonator", organDonator.isChecked());
                object.put("country", country.getText().toString());

                if (sexeBtn != null){
                    object.put("sexe", sexeBtn.getText().toString());
                }
                if (rbBloodChar != null && rbBloodRes != null){
                    object.put("bloodType",  rbBloodChar.getText().toString() + rbBloodRes.getText().toString() );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sendPostRequest(object, url);

        });

        //Cancel btn
        Button btnReturn = v.findViewById(R.id.newDMP_btnFormDmpReturn);
        btnReturn.setOnClickListener((View w) ->{
            getActivity().onBackPressed();
        });
        return v;
    }


    private void sendPostRequest(JSONObject request, String url){
        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest
                (Request.Method.POST, url, request, token, response -> {
                    ToasterMaker.makeToast(getContext(),"Dmp crée");
                    getActivity().onBackPressed();
                }, error -> {
                    if (error.networkResponse != null) {
                        switch (error.networkResponse.statusCode) {
                            case (409):
                                Log.e(LOG_TAG, error.toString());
                                ToasterMaker.makeToast(getContext(), "Security number already exist for another dmp");
                                break;
                            default:
                                ToasterMaker.makeToast(getContext(), "Error - Not created");
                                getActivity().onBackPressed();
                        }
                    }else{
                        ToasterMaker.makeToast(getContext(), "Fatal error - Not connected");
                    }
                });
        HttpSender.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRANCE);

        dateBirth.setText(sdf.format(myCalendar.getTime()));
    }
}
