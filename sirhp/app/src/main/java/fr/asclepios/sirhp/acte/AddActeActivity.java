package fr.asclepios.sirhp.acte;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.request.CustomJsonObjectRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class AddActeActivity extends AppCompatActivity {
    private int idDmp;
    private String token;
    private String urlServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        token = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE).getString("token", "");
        urlServer = getSharedPreferences("GLOBALS", MODE_PRIVATE).getString("url", null);
        setContentView(R.layout.activity_addacte);
        Button createActe = (Button) findViewById(R.id.acte_create);
        Button cancel = (Button) findViewById(R.id.acte_cancel);
        Bundle bundle = getIntent().getExtras();
        idDmp = getIntent().getIntExtra("idDmp", -1);


        createActe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(AddActeActivity.this, ActeActivity.class);
                Spinner typeActeSpinner = (Spinner) findViewById(R.id.acte_spinner_typeacte);
                String typeActe = typeActeSpinner.getSelectedItem().toString();
                boolean validationActe = false;
                int idPHDemand = 1;
                SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                String s = parseFormat.format(date);

                createActe( idDmp,  idPHDemand,  typeActe,  validationActe, s);
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(AddActeActivity.this, ActeActivity.class);
                startActivity(myIntent);
            }
        });
    }
    private void createActe(int idDMP, int idPHDemand, String typeActe, boolean validationActe, String date){
        JSONObject request = new JSONObject();
        try {
            request.put("idDmp", idDMP);
            request.put("idPHDemand", idPHDemand);
            request.put("typeActe", typeActe);
            request.put("validationActe", validationActe);
            request.put("dateValidation", date);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendPostRequest(request, urlServer+"acte/creation/");
    }

    private void sendPostRequest(JSONObject request, String url) {
        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest
                (Request.Method.POST, url, request, token,this::createActeResult, error -> {
                    ToasterMaker.makeToast(this, "Connection Error:" + error );

                    Log.e("ERROR","" + error);

                });

        // Access the RequestQueue through your singleton class.
        HttpSender.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }
    private void createActeResult(JSONObject response){
        if(response.isNull("idActe")){
            ToasterMaker.makeToast(this, "Acte non créer");
        }
        else{
            ToasterMaker.makeToast(this, "Acte bien créer");
            Intent myIntent = new Intent(AddActeActivity.this, ShowActeActivity.class);
            try {
                String typeActeAndDate = response.getString("typeActe") + " " +  response.getString("dateValidation").substring(0,10);
                myIntent.putExtra("idActe", Integer.valueOf(response.getString("idActe")));
                myIntent.putExtra("typeActeAndDate", typeActeAndDate);
                myIntent.putExtra("validationActe", response.getBoolean("validationActe"));
                startActivity(myIntent);
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                ToasterMaker.makeToast(this, "Erreur lors de la création de l'acte !");
            }
        }
    }
}
