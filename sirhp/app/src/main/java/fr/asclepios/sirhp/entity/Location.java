package fr.asclepios.sirhp.entity;

import java.util.Date;
import java.util.List;

import fr.asclepios.sirhp.entity.enumeration.DocumentType;


public class Location {
    private Double latitude;
    private Double longitude;
    private Date time;
    private String fname;
    private String lname;
    private List<String> roles;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return  "user : " + fname + " " + lname +  ", roles : " + roles +  ", latitude : " + latitude + ", longitude : " + longitude + ", time : " + time;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this) return true;
        if(!(obj instanceof Location)) return false;
        Location location = (Location) obj;
        return location.fname.equals(fname) && location.lname.equals(lname);
    }
}
