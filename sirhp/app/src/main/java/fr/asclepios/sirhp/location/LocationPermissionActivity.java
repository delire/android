package fr.asclepios.sirhp.location;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;

import java.util.List;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class LocationPermissionActivity extends AppCompatActivity implements PermissionsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_permission);
        PermissionsManager permissionsManager = new PermissionsManager(this);
        permissionsManager.requestLocationPermissions(this);
    }


    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        ToasterMaker.makeToast(this,  R.string.location_permission_not_granted);
        Toast.makeText(this, R.string.location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        final Intent intent = new Intent(this, LocationService.class);
        intent.setAction(LocationService.ACTION_LOCATION_PERMISSION);
        intent.putExtra(LocationService.LOCATION_PERMISSION, granted);
        startService(intent);
        finish();
    }
}
