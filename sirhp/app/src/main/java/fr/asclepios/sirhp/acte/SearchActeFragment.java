package fr.asclepios.sirhp.acte;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.entity.Acte;
import fr.asclepios.sirhp.request.CustomJsonArrayRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class SearchActeFragment extends Fragment {

    private Button addAct;
    private ListView lv;
    private boolean dateBoll;
    private Button mBtn;
    private Button searchBtn;
    private Button cancelBtn;
    private Button clearDate;
    private Button displayAll;
    private String token;
    private String urlServer;
    private String date;
    private Calendar c;
    private int idDmpg = -1;
    private DatePickerDialog dpd;
    private ArrayAdapter<String> adapter;
    private ArrayList<Acte> actesIdValide = new ArrayList<>();
    private ArrayList<String>acte;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.activity_searchacte, container, false);
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        urlServer = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);

        mBtn = (Button) v.findViewById(R.id.acte_datepicker);
        searchBtn = (Button) v.findViewById(R.id.acte_btn_search);
        cancelBtn = (Button) v.findViewById(R.id.acte_btn_search_cancel);
        clearDate = (Button) v.findViewById(R.id.acte_clear_date);
        displayAll = (Button) v.findViewById(R.id.acte_display_all);
        addAct = v.findViewById(R.id.acte_create);
        lv = v.findViewById(R.id.acte_list_result);
        try {
            int idDmp = getArguments().getInt("idDmp");
            idDmpg = idDmp;
            if (idDmp != 0) {
                searchActeByIdDmp(idDmp);
                searchBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dateBoll = false;
                        Spinner typeActeSpinner = (Spinner) v.findViewById(R.id.acte_spinner_typeacte_search);
                        String typeActe = typeActeSpinner.getSelectedItem().toString();
                        date = mBtn.getText().toString();
                        if(date.length() == 21 && typeActe.length() == 0){
                            ToasterMaker.makeToast(getContext(), "Veuillez renseigner au moins un des deux champs");
                            return;
                        }
                        if(date.length() != 21 && typeActe.length() != 0){
                            searchActeByDmpAndByType(idDmp, typeActe);
                            dateBoll = true;
                        }else{
                            if(date.length() != 21){
                                searchActeByIdDmp(idDmp);
                                dateBoll = true;
                            }else if(typeActe.length() != 0){
                                searchActeByDmpAndByType(idDmp, typeActe);
                            }
                        }

                    }
                });
            }
        }catch (Exception e){
            searchBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Spinner typeActeSpinner = (Spinner) v.findViewById(R.id.acte_spinner_typeacte_search);
                    String typeActe = typeActeSpinner.getSelectedItem().toString();
                    date = mBtn.getText().toString();
                    if(date.length() == 21 && typeActe.length() == 0){
                        ToasterMaker.makeToast(getContext(), "Veuillez renseigner au moins un des deux champs");
                        return;
                    }
                    if(date.length() != 21 && typeActe.length() != 0){
                        searchActeByType(typeActe);
                        dateBoll = true;
                    }else{
                        if(date.length() != 21){
                            dateBoll = true;
                            getAll();

                        }else if(typeActe.length() != 0){
                            searchActeByType(typeActe);
                        }
                    }

                }
            });
        }
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        clearDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date = "Sélectionner une date";
                mBtn.setText(date);
                dateBoll = false;
            }
        });
        if(idDmpg == 0){
            displayAll.setVisibility(View.INVISIBLE);
        }
        displayAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchActeByIdDmp(idDmpg);
                dateBoll = false;
                date = "Sélectionner une date";
                mBtn.setText(date);
            }
        });

        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = Calendar.getInstance();
                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                dpd = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int mYear, int mMonth, int mDay) {
                        mMonth ++;
                        if (mMonth < 10){
                            if(mDay < 10){
                                mBtn.setText(mYear+"-0"+mMonth+"-0"+mDay);
                            }else{
                                mBtn.setText(mYear+"-"+"0"+mMonth+"-"+mDay);
                            }
                        }
                        else{
                            if(mDay < 10){
                                mBtn.setText(mYear+"-"+mMonth+"-"+"-0"+mDay);
                            }else{
                                mBtn.setText(mYear+"-"+mMonth+"-"+"-"+mDay);
                            }
                        }

                    }
                }, year, month, day);
                dpd.show();
            }
        });

        addAct.setOnClickListener((View w) ->{
            Intent i = new Intent(getContext(), AddActeActivity.class);
            i.putExtra("idDmp",idDmpg );
            startActivityForResult(i,1);
        });

        return v;
    }

    private void searchActeByDmpAndByType(int idDmp, String typeActe){
        actesIdValide = new ArrayList<Acte>();
        JSONObject request = new JSONObject();
        sendPostRequest(request, urlServer+"acte/dmp/"+idDmp+"/type/"+typeActe);
    }
    private void searchActeByType(String typeActe){
        actesIdValide = new ArrayList<Acte>();
        JSONObject request = new JSONObject();
        sendPostRequest(request, urlServer+"acte/type/"+typeActe);
    }

    private void searchActeByIdDmp(int idDmp){
        actesIdValide = new ArrayList<Acte>();
        JSONObject request = new JSONObject();
        sendPostRequest(request, urlServer+"acte/dmp/"+idDmp);
    }

    private void getAll(){
        actesIdValide = new ArrayList<Acte>();
        JSONObject request = new JSONObject();
        sendPostRequest(request, urlServer+"acte/all");
    }

    private void sendPostRequest(JSONObject request, String url) {
        CustomJsonArrayRequest jsonArrayRequest = new CustomJsonArrayRequest
                (url, request, token, this::searchActeResult, error -> {
                    if (error != null)
                        switch(error.networkResponse.statusCode) {
                            case (404):
                                ToasterMaker.makeToast(getContext(), "Nothing here");
                                break;
                            default:
                                ToasterMaker.makeToast(getContext(), "Connection Error:" + error);
                                Log.e("ERROR", "" + error);
                        }
                });

        HttpSender.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);
    }
    private void searchActeResult(JSONArray response) {
        acte = new ArrayList<>();
        for(int i=0;i<response.length();i++) {
            try {
                JSONObject acteJson = response.getJSONObject(i);
                String typeActe = acteJson.getString("typeActe");
                String date = acteJson.getString("dateValidation");
                boolean validationActe = acteJson.getBoolean("validationActe");
                actesIdValide.add(new Acte(Integer.valueOf(acteJson.getString("idActe")),validationActe));
                String idDmp = acteJson.getString("idDmp");
                acte.add(typeActe + " " + date.substring(0,10));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, acte);

        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if(dateBoll){
            for(int y = 0; y<1000;y++){
                if(acte.size() != 0){
                    for(int i = 0; i<acte.size(); i++){
                        if(!acte.get(i).contains(date)) {
                            acte.remove(i);
                        }
                    }
                }
            }
            adapter.notifyDataSetChanged();
        }

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int idActe = actesIdValide.get(position).getIdActe();
                String typeActeAndDate = acte.get(position);
                Intent myIntent = new Intent(getContext(), ShowActeActivity.class);
                myIntent.putExtra("idActe", idActe);
                myIntent.putExtra("typeActeAndDate", typeActeAndDate);
                myIntent.putExtra("validationActe", actesIdValide.get(position).isValidationActe());
                startActivity(myIntent);
            }
        });
    }
}