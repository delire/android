package fr.asclepios.sirhp.document;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.entity.Document;

public class DocumentAdapter extends ArrayAdapter<Document> {
    public DocumentAdapter(Context context, int resource, List<Document> objects) {
        super(context, resource, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Document document = getItem(position);
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        TextView filenameView = convertView.findViewById(R.id.filename);
        filenameView.setText(document.getFilename());
        ImageView isDraftView = convertView.findViewById(R.id.isDraftView);
        if (document.getValidation()) isDraftView.setVisibility(View.INVISIBLE);
        return convertView;
    }
}
