package fr.asclepios.sirhp.utils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

public class ToasterMaker {


    public static void makeToast(Context ctx, String text) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(ctx, text, duration);
        toast.show();
    }

    public static void makeToast(Context ctx, @StringRes int resId) {
       Toast.makeText(ctx, resId, Toast.LENGTH_LONG).show();
    }
}
