
package fr.asclepios.sirhp.document;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.acte.ShowActeActivity;
import fr.asclepios.sirhp.entity.Document;
import fr.asclepios.sirhp.request.CustomJsonArrayRequest;
import fr.asclepios.sirhp.request.CustomJsonObjectRequest;
import fr.asclepios.sirhp.request.CustomStringRequest;
import fr.asclepios.sirhp.request.DataPart;
import fr.asclepios.sirhp.request.FileRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.DialogMaker;
import fr.asclepios.sirhp.utils.DocUtil;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class DocumentActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    private static final String LOG_TAG = DocumentActivity.class.getSimpleName();
    private static final int READ_REQUEST_CODE = 42;
    private static final int WRITE_PERMISSION_REQUEST_CODE = 2;
    private static final int READ_PERMISSION_REQUEST_CODE = 3;

    private String url;
    private String token;

    private static final int CREATE = 1;
    private static final int UPDATE = 2;

    private DownloadManager downloadManager;
    private List<Document> documents;
    private Document document;
    private long downloadID;
    private Gson gson;
    private int operation;

    private ListView listView;
    private DocumentAdapter documentAdapter;
    private DocumentDialog documentDialog;

    private int idActe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_document);
        String baseUrl = getSharedPreferences("GLOBALS", MODE_PRIVATE).getString("url", null);
        url = baseUrl + "documents/";
        token = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE).getString("token", "");
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        gson = new Gson();
        idActe = getIntent().getIntExtra(ShowActeActivity.ID_ACTE, -1);
        load();
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void open() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] mimeTypes = {"image/*", "audio/*", "video/*", "application/msword", "application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (intent != null) {
                try {
                    DataPart dataPart = getDataPart(intent.getData());
                    switch (operation) {
                        case CREATE:
                            create(dataPart);
                            break;
                        case UPDATE:
                            update(document, dataPart);
                            break;
                        default:
                            Log.e(LOG_TAG, "Unknown operation");
                    }
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Error while reading file : " + e.getMessage());
                }
            }
        }
    }

    private DataPart getDataPart(Uri uri) throws IOException {
        Log.d(LOG_TAG, "Get data part");

        DataPart dataPart = new DataPart();
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        cursor.moveToFirst();
        dataPart.setFileName(cursor.getString(nameIndex));

        InputStream inputStream = getContentResolver().openInputStream(uri);
        dataPart.setContent(DocUtil.readBytes(inputStream));

        dataPart.setType(getContentResolver().getType(uri));

        Log.d(LOG_TAG, dataPart.toString());
        return dataPart;
    }

    private void load() {
        Log.d(LOG_TAG, "Loading documents");
        CustomJsonArrayRequest docListRequest = new CustomJsonArrayRequest(url + "acte/" + idActe, null, token, response -> {
            documents = new ArrayList<Document>(Arrays.asList(gson.fromJson(response.toString(), Document[].class)));
            documentAdapter = new DocumentAdapter(DocumentActivity.this, R.layout.list_item, documents);
            listView.setAdapter(documentAdapter);
            if (documents.isEmpty())
                ToasterMaker.makeToast(DocumentActivity.this, R.string.doc_msg_empty_list);
        }, error -> {
            Log.e(LOG_TAG, error.toString());
            ;
            ToasterMaker.makeToast(DocumentActivity.this, R.string.doc_msg_empty_list);
        });
        HttpSender.getInstance(this).addToRequestQueue(docListRequest);
    }

    private void create(DataPart dataPart) {
        Log.d(LOG_TAG, "Create document");
        FileRequest createRequest = new FileRequest(dataPart, url + "acte/" + idActe, token, response -> {
            try {
                String jsonString =
                        new String(
                                response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                documents.add(gson.fromJson(jsonString, Document.class));
                documentAdapter.notifyDataSetChanged();
            } catch (UnsupportedEncodingException e) {
                Log.e(LOG_TAG, "Error while parsing response : " + e.getMessage());
            }
        }, error -> {
            int msgResId;
            Log.e(LOG_TAG, error.toString());
            switch (error.networkResponse.statusCode) {
                case 500:
                    msgResId = R.string.doc_msg_empty_list;
                    break;
                case 501:
                    msgResId = R.string.doc_msg_invalid_error;
                    break;
                default:
                    msgResId = R.string.doc_msg_unknown_error;
            }
            ToasterMaker.makeToast(DocumentActivity.this, msgResId);
        });
        HttpSender.getInstance(this).addToRequestQueue(createRequest);
    }

    private void update(Document document, DataPart dataPart) {
        Log.d(LOG_TAG, "Update document");
        FileRequest updateRequest = new FileRequest(dataPart, url + document.getId(), token, response -> {
            try {
                String jsonString =
                        new String(
                                response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                Document updatedDoc = gson.fromJson(jsonString, Document.class);
                documents.set(documents.indexOf(document), updatedDoc);
                documentAdapter.notifyDataSetChanged();
                documentDialog.show(updatedDoc);
            } catch (UnsupportedEncodingException e) {
                Log.e(LOG_TAG, "Error while parsing response : " + e.getMessage());
            }
        }, error -> {
            int msgResId;
            Log.e(LOG_TAG, error.toString());
            ;
            switch (error.networkResponse.statusCode) {
                case 403:
                    msgResId = R.string.doc_msg_update_error;
                    break;
                case 404:
                    msgResId = R.string.doc_msg_not_found_error;
                    break;
                case 500:
                    msgResId = R.string.doc_msg_io_error;
                    break;
                default:
                    msgResId = R.string.doc_msg_unknown_error;
            }
            ToasterMaker.makeToast(DocumentActivity.this, msgResId);
        });
        HttpSender.getInstance(this).addToRequestQueue(updateRequest);
    }

    public void download(Document document) {
        Uri uri = Uri.fromFile(new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), document.getFilename()));
        DownloadManager.Request downloadRequest = new DownloadManager.Request(Uri.parse(url + "download/" + document.getId()));
        downloadRequest.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |
                DownloadManager.Request.NETWORK_MOBILE)
                .setVisibleInDownloadsUi(true)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .addRequestHeader("Authorization", "Bearer " + token)
                .setDestinationUri(uri);
        downloadID = downloadManager.enqueue(downloadRequest);
    }

    public void validate(Document document) {
        Log.d(LOG_TAG, "Validate document");
        CustomJsonObjectRequest validateRequest = new CustomJsonObjectRequest(Request.Method.PUT, url + "validate/" + document.getId(), null, token, response -> {
            Document doc = gson.fromJson(response.toString(), Document.class);
            documents.set(documents.indexOf(doc), doc);
            documentAdapter.notifyDataSetChanged();
            documentDialog.show(doc);
            ToasterMaker.makeToast(DocumentActivity.this, R.string.doc_msg_validated);
        }, error -> {
            int msgResId;
            Log.e(LOG_TAG, error.toString());
            switch (error.networkResponse.statusCode) {
                case 404:
                    msgResId = R.string.doc_msg_not_found_error;
                    break;
                default:
                    msgResId = R.string.doc_msg_unknown_error;
            }
            ToasterMaker.makeToast(DocumentActivity.this, msgResId);
        });
        HttpSender.getInstance(this).addToRequestQueue(validateRequest);
    }

    public void delete(Document document) {
        Log.d(LOG_TAG, "Delete document");
        CustomStringRequest deleteRequest = new CustomStringRequest(Request.Method.DELETE, url + document.getId(), token, response -> {
            documents.remove(document);
            documentAdapter.notifyDataSetChanged();
            ToasterMaker.makeToast(DocumentActivity.this, R.string.doc_msg_deleted);
            documentDialog.dismiss();
        }, error -> {
            int msgResId;
            Log.e(LOG_TAG, error.toString());
            ;
            switch (error.networkResponse.statusCode) {
                case 403:
                    msgResId = R.string.doc_msg_delete_error;
                    break;
                case 404:
                    msgResId = R.string.doc_msg_not_found_error;
                    break;
                case 500:
                    msgResId = R.string.doc_msg_io_error;
                    break;
                default:
                    msgResId = R.string.doc_msg_unknown_error;
            }
            ToasterMaker.makeToast(DocumentActivity.this, msgResId);
        });
        HttpSender.getInstance(this).addToRequestQueue(deleteRequest);
    }

    public void createBtnHandler(View view) {
        operation = CREATE;
        checkReadPermission();
    }

    private void checkReadPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)
                open();
            else
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_PERMISSION_REQUEST_CODE);
        } else //permission is automatically granted on sdk<23 upon installation
            open();
    }

    private void checkWritePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)
                download(document);
            else
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_PERMISSION_REQUEST_CODE);
        } else
            download(document);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case WRITE_PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) download(document);
                break;

            case READ_PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) open();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(LOG_TAG, "Document item clicked");
        document = (Document) parent.getItemAtPosition(position);
        documentDialog = new DocumentDialog(this, document);
        documentDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.downloadBtn:
                checkWritePermission();
                break;
            case R.id.updateBtn:
                operation = UPDATE;
                checkReadPermission();
                break;
            case R.id.validateBtn:
                DialogMaker.makeDialog(this, R.string.doc_validate, R.string.doc_dlg_validate_msg, (DialogInterface dialog, int which) -> validate(document));
                break;
            case R.id.deleteBtn:
                DialogMaker.makeDialog(this, R.string.doc_delete, R.string.doc_dlg_delete_msg, (DialogInterface dialog, int which) -> delete(document));
                break;
            default:
                documentDialog.dismiss();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(LOG_TAG, "Download complete");
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L);
            if (id != downloadID) {
                Log.v(LOG_TAG, "Ingnoring unrelated download " + id);
                return;
            }

            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(id);
            Cursor cursor = downloadManager.query(query);

            if (!cursor.moveToFirst()) {
                Log.e(LOG_TAG, "Empty row");
                return;
            }

            int statusIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
            if (DownloadManager.STATUS_SUCCESSFUL != cursor.getInt(statusIndex)) {
                Log.w(LOG_TAG, "Download Failed");
                return;
            }

            int uriIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI);
            String uriString = cursor.getString(uriIndex);

            Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", new File(Uri.parse(uriString).getPath()));
            Intent viewIntent = new Intent(Intent.ACTION_VIEW);
            viewIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            viewIntent.setDataAndType(uri, document.getContentType());
            Intent chooserIntent = Intent.createChooser(viewIntent, "Ouvrir le document");
            context.startActivity(chooserIntent);
        }
    };
}
