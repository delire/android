package fr.asclepios.sirhp.dmp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.request.CustomJsonObjectRequestArrayResponse;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class FindDmpFragment extends Fragment {

    public static String ARG_POSITION = "findDmpFrag_pos";

    private ListView lv;
    private EditText securityNumberET;
    private EditText lastNameET;
    private EditText firstNameET;
    private EditText idDmpET;
    private String token;
    private String baseUrl;

    public FindDmpFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.activity_find_dmp, container, false);
        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        baseUrl = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);

        securityNumberET = v.findViewById(R.id.dmpSearch_findSecurityNumberET);
        lastNameET = v.findViewById(R.id.dmpSearch_findLastNameET);
        idDmpET = v.findViewById(R.id.dmpSearch_idDmpET);
        firstNameET = v.findViewById(R.id.dmpSearch_findFirstNameET);

        Button btnFindDmp = v.findViewById(R.id.dmpSearch_btnSendFind);
        btnFindDmp.setOnClickListener((View w) -> {
            //hideKeyboard();


            JSONObject request = new JSONObject();
            try {
                request.put("securityNumber", securityNumberET.getText().toString());
                request.put("lastName", lastNameET.getText().toString());
                request.put("firstName", firstNameET.getText().toString());
                request.put("idDmpET", idDmpET.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            StringBuilder sb = new StringBuilder();
            sb.append("?");
            sb.append("securityNumber=").append(securityNumberET.getText().toString());
            sb.append("&lastName=").append(lastNameET.getText().toString());
            sb.append("&firstName=").append(firstNameET.getText().toString());
            sb.append("&idDmp=").append(idDmpET.getText().toString());
            sendGetRequest(request, baseUrl + "dmp/findDmp"+sb.toString());

        });
        return v;
    }

    private void sendGetRequest(JSONObject request, String url) {
        CustomJsonObjectRequestArrayResponse jsonObjectRequest = new CustomJsonObjectRequestArrayResponse
                (Request.Method.GET, url, request, token, this::fillListView, error -> {
                    ToasterMaker.makeToast(getContext(), "Search Error");
                });
        HttpSender.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void fillListView(JSONArray response) {
        ArrayList<HashMap<String,String>>dmps = new ArrayList<>();
        HashMap<String,String> item;
        for(int i=0;i<response.length();i++) {
            try {
                JSONObject dmpJson = response.getJSONObject(i);
                item = new HashMap<String,String>();
                item.put( "id",  dmpJson.getString("idDmp"));
                if (!dmpJson.isNull("securityNumber")){
                    item.put( "sn",  dmpJson.getString("securityNumber"));
                }
                else{
                    item.put( "sn", "[Undefined]");
                }

                String name;
                if (!dmpJson.isNull("lastName")){
                    name = dmpJson.getString("lastName");
                }
                else{
                    name = "";
                }
                if (!dmpJson.isNull("firstName")){
                    name = name + " " + dmpJson.getString("firstName");
                }
                else{
                    name = name  + "";
                }
                item.put( "lnFn",  name.length() > 0 ? name : "[Undefined]" );
                dmps.add(item);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        SimpleAdapter adapter ;

        adapter = new SimpleAdapter(getContext(), dmps,
                R.layout.multi_lines_dmpsearch,
                new String[] { "id","sn", "lnFn" },
                new int[] {R.id.line_a, R.id.line_b, R.id.line_c});

        lv = getView().findViewById(R.id.dmpSearch_resultListView);
        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                HashMap<String,String> selected = (HashMap<String,String>) lv.getItemAtPosition(position);

                /*
                Intent i = new Intent(getActivity(), DmpConsultationFragment.class);
                Bundle b = new Bundle();
                b.putInt("id", Integer.parseInt(selected.get("id")));
                i.putExtras(b);
                startActivity(i);
                */
                DmpConsultationFragment dmpFragment = new DmpConsultationFragment();
                Bundle b = new Bundle();
                b.putInt("id", Integer.parseInt(selected.get("id")));
                dmpFragment.setArguments(b);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, dmpFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });
    }

    /*
    private void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    */
}
