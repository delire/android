package fr.asclepios.sirhp.allergy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.request.CustomJsonObjectRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class NewAllergyActivity extends AppCompatActivity {
    private static final String LOG_TAG = NewAllergyActivity.class.getSimpleName();

    private Button addBtn;
    private EditText name;
    private String token;
    private String baseUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove app bar
        getSupportActionBar().hide();
        setContentView(R.layout.activity_new_allergy);

        addBtn = findViewById(R.id.newAllergy_Validate);
        name = findViewById(R.id.newAllergy_nameET);

        token = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE).getString("token", "");
        baseUrl = getSharedPreferences("GLOBALS", MODE_PRIVATE).getString("url", null);

        addBtn.setOnClickListener(x -> {
            JSONObject object = new JSONObject();
            String url = baseUrl + "allergy/addAllergy";

            try {
                object.put("allergyName", name.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendPostRequest(object, url);
            Intent i = new Intent();
            setResult(1, i);
            finish();
        });

    }


    private void sendPostRequest(JSONObject request, String url){
        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest
                (Request.Method.POST, url, request, token, response -> {
                    Intent i = new Intent();
                    ToasterMaker.makeToast(this,"Allergy added");
                    setResult(RESULT_OK, i);
                    finish();
                }, error -> {
                    Intent i = new Intent();
                    ToasterMaker.makeToast(this,"Error happened");
                    Log.e(LOG_TAG, error.toString());
                    setResult(2, i);
                    finish();
                });
        HttpSender.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }
}
