package fr.asclepios.sirhp.dmp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.acte.SearchActeFragment;
import fr.asclepios.sirhp.allergy.AllergyPatientActivity;
import fr.asclepios.sirhp.request.CustomJsonObjectRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.DateMask;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class DmpConsultationFragment extends android.support.v4.app.Fragment {
    private static final String LOG_TAG = DmpConsultationFragment.class.getSimpleName();

    private TextView id;
    private TextView firstName;
    private TextView lastName;
    private TextView securityNumber;
    private TextView dateBirth;
    private TextView phoneNumber;
    private TextView phoneNumberSec;
    private TextView street;
    private TextView postal;
    private TextView city;
    private TextView mail;
    private Switch organDonator;
    private TextView country;
    private Button validateBtn;
    private Button allergyBtn;
    private Button actAccessBtn;
    private RadioGroup dgSexe;
    private RadioButton sexeBtn;

    private RadioGroup dgBloodChar;
    private RadioButton rbBloodChar;

    private RadioGroup dgBloodRes;
    private RadioButton rbBloodRes;

    private String token;
    private String baseUrl;

    private final Calendar myCalendar = Calendar.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_dmp_consultation, container, false);
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        baseUrl = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);

        id = v.findViewById(R.id.dmp_idDmpValueTV);
        firstName = v.findViewById(R.id.dmp_firstNameET);
        lastName = v.findViewById(R.id.dmp_lastNameET);
        securityNumber = v.findViewById(R.id.dmp_securityNumberET);
        dateBirth = v.findViewById(R.id.dmp_birthdayDATE);
        phoneNumber = v.findViewById(R.id.dmp_phoneNumberET);
        phoneNumberSec = v.findViewById(R.id.dmp_phoneNumberSecET);
        street = v.findViewById(R.id.dmp_streetET);
        postal = v.findViewById(R.id.dmp_postalET);
        city = v.findViewById(R.id.dmp_cityET);
        mail = v.findViewById(R.id.dmp_mailET);
        country = v.findViewById(R.id.dmp_countryET);
        organDonator = v.findViewById(R.id.dmp_donatorSwitch);
        validateBtn = v.findViewById(R.id.dmp_btnFormDmpValidate);
        dgSexe = v.findViewById(R.id.dmp_rgSexe);
        sexeBtn = v.findViewById(dgSexe.getCheckedRadioButtonId());
        allergyBtn = v.findViewById(R.id.dmp_allergyBtn);
        actAccessBtn = v.findViewById(R.id.dmp_actBtn);

        dgBloodChar = v.findViewById(R.id.dmp_rgBloodChar);
        rbBloodChar = v.findViewById(dgBloodChar.getCheckedRadioButtonId());

        dgBloodRes = v.findViewById(R.id.dmp_rgBloodRes);
        rbBloodRes = v.findViewById(dgBloodRes.getCheckedRadioButtonId());


        int value = -1;
        value = getArguments().getInt("id");

        JSONObject request = new JSONObject();
        sendGetRequest(request, baseUrl + "dmp/getDmp/" + value);

        actAccessBtn.setOnClickListener((View w) ->{
            SearchActeFragment searchActeFragment = new SearchActeFragment();
            Bundle b = new Bundle();
            b.putInt("idDmp", getArguments().getInt("id"));
            searchActeFragment.setArguments(b);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, searchActeFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        });

        dateBirth.addTextChangedListener(new DateMask());


        DatePickerDialog.OnDateSetListener dateSetListener = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };

        dateBirth.setOnClickListener(w -> {
            new DatePickerDialog(getContext(), dateSetListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        validateBtn.setOnClickListener((View w) -> {
            JSONObject object = new JSONObject();
            String validateUrl = baseUrl + "dmp/editDmp";
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            try {
                object.put("idDmp", Integer.parseInt(id.getText().toString()));
                object.put("firstName", firstName.getText().toString());
                object.put("lastName", lastName.getText().toString());
                object.put("securityNumber", securityNumber.getText().toString());
                if (dateBirth != null && !dateBirth.getText().toString().equals("")) {
                    Date date = formatter.parse(dateBirth.getText().toString());
                    ((SimpleDateFormat) formatter).applyPattern("dd/MM/yyyy");
                    object.put("dateOfBirth", formatter.format(date));
                }
                object.put("phoneNumberPatient", phoneNumber.getText().toString());
                object.put("phoneNumberSecPatient", phoneNumberSec.getText().toString());
                object.put("street", street.getText().toString());
                object.put("postal", postal.getText().toString());
                object.put("city", city.getText().toString());
                object.put("mailPatient", mail.getText().toString());
                object.put("organDonator", organDonator.isChecked());
                object.put("country", country.getText().toString());

                sexeBtn = v.findViewById(dgSexe.getCheckedRadioButtonId());
                rbBloodChar = v.findViewById(dgBloodChar.getCheckedRadioButtonId());
                rbBloodRes = v.findViewById(dgBloodRes.getCheckedRadioButtonId());

                if (sexeBtn != null) {
                    object.put("sexe", sexeBtn.getText().toString());
                }

                if (rbBloodChar != null && rbBloodRes != null) {
                    object.put("bloodType", rbBloodChar.getText().toString() + rbBloodRes.getText().toString());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
                return;
            }
            sendPostRequest(object, validateUrl);

        });

        allergyBtn.setOnClickListener((View w) -> {
            AllergyPatientActivity allergyPatientFragment = new AllergyPatientActivity();
            Bundle b = new Bundle();
            b.putInt("id", getArguments().getInt("id"));
            allergyPatientFragment.setArguments(b);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, allergyPatientFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        });
        return v;
    }

    private void sendGetRequest(JSONObject request, String url) {
        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest
                (Request.Method.GET, url, null, token, this::fillFields, error -> {
                    ToasterMaker.makeToast(getContext(), "Get dmp Error");
                    Log.e(LOG_TAG, error.toString());
                });
        HttpSender.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }


    private void fillFields(JSONObject response) {
        try {
            JSONObject dmpJson = response;

            if (!dmpJson.isNull("idDmp")) {
                id.setText(dmpJson.getString("idDmp"));
            }

            if (!dmpJson.isNull("firstName")) {
                firstName.setText(dmpJson.getString("firstName"));
            }
            if (!dmpJson.isNull("lastName")) {
                lastName.setText(dmpJson.getString("lastName"));
            }
            if (!dmpJson.isNull("securityNumber")) {
                securityNumber.setText(dmpJson.getString("securityNumber"));
            }
            if (!dmpJson.isNull("dateOfBirth")) {
                dateBirth.setText(dmpJson.getString("dateOfBirth"));
            }
            if (!dmpJson.isNull("phoneNumberPatient")) {
                phoneNumber.setText(dmpJson.getString("phoneNumberPatient"));
            }
            if (!dmpJson.isNull("phoneNumberSecPatient")) {
                phoneNumberSec.setText(dmpJson.getString("phoneNumberSecPatient"));
            }
            if (!dmpJson.isNull("street")) {
                street.setText(dmpJson.getString("street"));
            }
            if (!dmpJson.isNull("postal")) {
                postal.setText(dmpJson.getString("postal"));
            }
            if (!dmpJson.isNull("city")) {
                city.setText(dmpJson.getString("city"));
            }
            if (!dmpJson.isNull("country")) {
                country.setText(dmpJson.getString("country"));
            }
            if (!dmpJson.isNull("mailPatient")) {
                mail.setText(dmpJson.getString("mailPatient"));
            }
            if (!dmpJson.isNull("organDonator")) {
                organDonator.setChecked(dmpJson.getBoolean("organDonator"));
            }

            if (!dmpJson.isNull("sexe")) {
                String sexe = dmpJson.getString("sexe");
                if (sexe.equals("Homme")) {
                    dgSexe.check(R.id.dmp_sexeBtnHomme);
                } else {
                    dgSexe.check(R.id.dmp_sexeBtnFemme);
                }
            }

            if (!dmpJson.isNull("bloodType")) {
                String bloodType = dmpJson.getString("bloodType");
                String group = bloodType.substring(0, bloodType.length() - 1);
                String rhesus = bloodType.substring(bloodType.length() - 1, bloodType.length());
                switch (group) {
                    case "A":
                        dgBloodChar.check(R.id.dmp_bloodA);
                        break;
                    case "B":
                        dgBloodChar.check(R.id.dmp_bloodB);
                        break;
                    case "AB":
                        dgBloodChar.check(R.id.dmp_bloodAB);
                        break;
                    case "O":
                        dgBloodChar.check(R.id.dmp_bloodO);
                        break;
                    default:
                        //Nothing
                }
                switch (rhesus) {
                    case "+":
                        dgBloodRes.check(R.id.dmp_bloodPositif);
                        break;
                    case "-":
                        dgBloodRes.check(R.id.dmp_bloodNeg);
                        break;
                    default:
                        //Nothing
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void sendPostRequest(JSONObject request, String url) {
        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest
                (Request.Method.POST, url, request, token, response -> {
                    Intent i = new Intent();
                    ToasterMaker.makeToast(getContext(), "Dmp mis à jour");
                    getActivity().onBackPressed();
                }, error -> {
                    Intent i = new Intent();
                    switch(error.networkResponse.statusCode){
                        case (404) :
                            Log.e(LOG_TAG, error.toString());
                            ToasterMaker.makeToast(getContext(), "Dmp not found");
                            break;
                        case (409):
                            Log.e(LOG_TAG, error.toString());
                            ToasterMaker.makeToast(getContext(), "Security number already exist for another dmp");
                            break;
                        default:
                            ToasterMaker.makeToast(getContext(), "Error - Not updated");
                            getActivity().onBackPressed();
                    }


                });
        // Access the RequestQueue through your singleton class.
        HttpSender.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRANCE);

        dateBirth.setText(sdf.format(myCalendar.getTime()));
    }

}
