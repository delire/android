package fr.asclepios.sirhp.user;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import fr.asclepios.sirhp.request.CustomJsonArrayRequest;
import fr.asclepios.sirhp.request.CustomJsonObjectRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class CreateUserFragment extends Fragment {
    private String token;
    private EditText dateEdit;
    private DatePickerDialog datePickerDialog;
    private EditText firstName;
    private EditText lastName;
    private EditText street;
    private EditText city;
    private EditText postalCode;
    private EditText country;
    private EditText phone1;
    private EditText phone2;
    private EditText email;
    private EditText password;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private ListView lv;
    private Spinner spinnerRole;
    private ArrayList<String> rolesNames = new ArrayList<String>();
    private ArrayList<LinkedHashMap<String,String>> roles  = new ArrayList<LinkedHashMap<String,String>>();
    private List<LinkedHashMap<String, String>> roleChoice  = new ArrayList<LinkedHashMap<String,String>>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.activity_create_user, container, false);
        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        String urlServer = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);

        spinnerRole = (Spinner) v.findViewById(R.id.spinnerRegion);

        // initiate the date picker and a button
        //date =  findViewById(R.id.date);
        Button btnNewUser = v.findViewById(R.id.button_create);

        getAllRole(urlServer+"getAllRoles");

        btnNewUser.setOnClickListener((View w) -> {
            JSONObject object = new JSONObject();
            JSONArray arrayChoice = new JSONArray();
            JSONObject roleC = new JSONObject();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");

            firstName = v.findViewById(R.id.first_name_edit);
            lastName = v.findViewById(R.id.last_name_edit);
            street = v.findViewById(R.id.rue_edit);
            country = v.findViewById(R.id.country_edit);
            city = v.findViewById(R.id.ville_edit);
            postalCode = v.findViewById(R.id.code_postal_edit);
            phone1 = v.findViewById(R.id.phone1_edit);
            phone2 = v.findViewById(R.id.phone2_edit);
            email = v.findViewById(R.id.email_edit);
            password = v.findViewById(R.id.password_edit);
            radioGroup = (RadioGroup) v.findViewById(R.id.genderRadioGroup);
            // get selected radio button from radioGroup
            int selectedId = radioGroup.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            radioButton = (RadioButton) v.findViewById(selectedId);
            try {
                try{
                    Date localDate = formatter.parse(dateEdit.getText().toString());
                    object.put("birthday", formatDate.format(localDate));
                }catch(ParseException e){
                    object.put("birthday", dateEdit.getText().toString());

                }

                object.put("firstName",firstName.getText().toString());
                object.put("lastName", lastName.getText().toString());
                if (radioButton != null) {
                    object.put("sexe", radioButton.getText().toString());
                }
                object.put("postalCode", postalCode.getText().toString());
                object.put("phonePrimary", phone1.getText().toString());
                object.put("phoneSecondary", phone2.getText().toString());
                object.put("city", city.getText().toString());
                object.put("country", country.getText().toString());
                object.put("email", email.getText().toString());
                object.put("street", street.getText().toString());
                object.put("password", password.getText().toString());


                roleC.put("roleId", roleChoice.get(0).get("roleId"));
                roleC.put("roleName", roleChoice.get(0).get("roleName"));
                arrayChoice.put(roleC);

                object.put("addedIdRole", arrayChoice);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendPostRequest(object, urlServer+"createUser");
        });

        dateEdit =  v.findViewById(R.id.birthday_edit);
        // perform click event on edit text
        dateEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                StringBuilder sb = new StringBuilder();
                sb.append( Calendar.YEAR);
                c.set(c.get(Calendar.YEAR) - 18, c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                dateEdit.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        spinnerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String myRole = String.valueOf(spinnerRole.getSelectedItem());
                LinkedHashMap l = new LinkedHashMap<>();
                l.put("roleId",roles.get( spinnerRole.getSelectedItemPosition()).get("roleId"));
                l.put("roleName",myRole);
                roleChoice.clear();
                roleChoice.add(l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });
        return v;
    }

    private void sendPostRequest(JSONObject request, String url){
        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest
                (Request.Method.POST, url, request, token, response -> {
                    Intent i = new Intent();
                    ToasterMaker.makeToast(getContext(),"Utilisateur crée");
                    getActivity().onBackPressed();
                }, error -> {
                    Intent i = new Intent();
                    ToasterMaker.makeToast(getContext(),"fail");
                });

        HttpSender.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void getAllRole(String url){
        RequestQueue queue = Volley.newRequestQueue(getContext());
        CustomJsonArrayRequest jsonArrayRequest = new CustomJsonArrayRequest
                (Request.Method.GET, url, null, token, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try{
                            // Loop through the array elements
                            for(int i=0;i<response.length();i++){
                                LinkedHashMap l  = new LinkedHashMap<>();

                                // Get current json object
                                JSONObject student = response.getJSONObject(i);

                                // Get the current student (json object) data
                                String roleId = student.getString("roleId");
                                String roleName = student.getString("roleName");
                                l.put("roleId",roleId);
                                l.put("roleName",roleName);
                                rolesNames.add(roleName);
                                roles.add(l);
                            }
                            ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item,rolesNames);
                            dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerRole.setAdapter(dataAdapterR);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        HttpSender.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);

    }
}
