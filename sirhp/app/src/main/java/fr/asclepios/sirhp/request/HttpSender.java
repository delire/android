package fr.asclepios.sirhp.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class HttpSender {
    private static HttpSender instanceSender;
    private RequestQueue queue;
    private Context ctx;

    public HttpSender(Context ctx) {
        this.ctx = ctx;
        queue = Volley.newRequestQueue(ctx);

    }

    public static synchronized HttpSender getInstance(Context context) {
        if (instanceSender == null) {
            instanceSender = new HttpSender(context);
        }
        return instanceSender;
    }


    public RequestQueue getRequestQueue() {
        if (queue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            queue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return queue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }


}