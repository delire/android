package fr.asclepios.sirhp.entity;

import java.util.Date;

import fr.asclepios.sirhp.entity.enumeration.DocumentType;


public class Document {
    private Long id;
    private String basename;
    private String extension;
    private DocumentType type;
    private String contentType;
    private Boolean validation;
    private Date dateCreation;
    private Date dateModification;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBasename() {
        return basename;
    }

    public void setBasename(String basename) {
        this.basename = basename;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public String getFilename() {
        return this.basename + "." + this.extension;
    }

    @Override
    public String toString() {
        return "id : " + this.id + ", filename : " + getFilename();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this) return true;
        if(!(obj instanceof Document)) return false;
        Document document = (Document) obj;
        return document.id.equals(id);
    }
}
