package fr.asclepios.sirhp.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import fr.asclepios.sirhp.R;

public class DialogMaker {

    public static void makeDialog(Context context, int titleResId, int msgResId, DialogInterface.OnClickListener onPositiveBtn) {
        new AlertDialog.Builder(context)
                .setTitle(titleResId)
                .setMessage(msgResId)
                .setPositiveButton(R.string.yes, onPositiveBtn)
                .setNegativeButton(R.string.cancel, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
