package fr.asclepios.sirhp.user;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.request.CustomJsonArrayRequest;
import fr.asclepios.sirhp.request.CustomJsonObjectRequest;
import fr.asclepios.sirhp.request.CustomStringRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.MultiSelectionSpinner;
import fr.asclepios.sirhp.utils.ToasterMaker;

import static java.lang.Integer.parseInt;


public class SelfProfilFragment extends Fragment {


    public SelfProfilFragment() {
        // Required empty public constructor
    }


    private String token;
    private TextView dateEdit;
    private DatePickerDialog datePickerDialog;
    private TextView firstName;
    private TextView lastName;
    private TextView street;
    private TextView city;
    private TextView postalCode;
    private String id;
    private TextView country;
    private TextView phone1;
    private TextView phone2;
    private TextView email;
    private TextView password;
    private RadioGroup radioGroup;
    private RadioButton radioButton;

    private RadioGroup genderRadioGroup;
    private RadioButton femaleRadio;
    private RadioButton maleRadio;

    private MultiSelectionSpinner spinnerRole;
    private ArrayList<String> rolesNames = new ArrayList<String>();
    private ArrayList<LinkedHashMap<String,String>> roles  = new ArrayList<LinkedHashMap<String,String>>();
    private HashMap<Integer, String> positionToRole  = new HashMap<Integer,String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_self_profil, container, false);
        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        String urlServer = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE);


        id = (String) pref.getString("id", "");

        firstName = v.findViewById(R.id.self_first_name_edit);
        lastName = v.findViewById(R.id.self_last_name_edit);
        street = v.findViewById(R.id.self_rue_edit);
        country = v.findViewById(R.id.self_country_edit);
        city = v.findViewById(R.id.self_ville_edit);
        postalCode = v.findViewById(R.id.self_code_postal_edit);
        phone1 = v.findViewById(R.id.self_phone1_edit);
        phone2 = v.findViewById(R.id.self_phone2_edit);
        email = v.findViewById(R.id.self_email_edit);
        password = v.findViewById(R.id.self_password_edit);
        maleRadio = v.findViewById(R.id.self_maleRadioButton);
        femaleRadio = v.findViewById(R.id.self_maleRadioButton);

        dateEdit =  v.findViewById(R.id.self_profil_birthday_edit);
        // perform click event on edit text

        getUser(urlServer+"printUser/"+id);


        return v;
    }



    private void getUser(String url){
        // Request a string response from the provided URL.
        CustomStringRequest stringRequest = new CustomStringRequest(Request.Method.GET, url, token,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONObject json= (JSONObject) new JSONTokener(response).nextValue();
                            JSONArray json2 = json.getJSONArray("gotRole");
                            ArrayList<Integer> rabJ = new ArrayList<>();
                            for(int i = 0 ; i < json2.length(); i++){
                                JSONObject tmpSon = json2.getJSONObject(i);
                                for(int j = 0; j < roles.size(); j++){
                                    if(parseInt(roles.get(j).get("roleId")) == parseInt(tmpSon.get("role").toString())){
                                        rabJ.add(j);
                                    }
                                }
                            }
                            firstName.setText(json.get("firstName").toString());
                            lastName.setText(json.get("lastName").toString());
                            street.setText(json.get("street").toString());
                            city.setText(json.get("city").toString());
                            postalCode.setText(json.get("postalCode").toString());
                            country.setText(json.get("country").toString());
                            phone1.setText(json.get("phonePrimary").toString());
                            phone2.setText(json.get("phoneSecondary").toString());
                            email.setText(json.get("email").toString());
                            password.setText("**********");
                            dateEdit.setText(json.get("birthday").toString());

                            if(json.get("sexe").toString().toLowerCase().equals("homme")){

                                maleRadio.setChecked(true);
                            }else{
                                femaleRadio.setChecked(true);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        HttpSender.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

}
