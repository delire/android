package fr.asclepios.sirhp.allergy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.request.CustomJsonObjectRequestArrayResponse;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class AllergyPatientActivity extends Fragment {
    private static final String LOG_TAG = AllergyPatientActivity.class.getSimpleName();

    private ListView lv;
    private Button addAllergy;
    private String token;
    private String baseUrl;

    @Override
    public void onResume() {
        int value = -1; // or other values

        value = getArguments().getInt("id");
        JSONObject request = new JSONObject();
        sendGetLoadRequest(request, baseUrl + "allergy/getAllergyPatient/" + value);
        super.onResume();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.activity_allergy_patient, container, false);
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        baseUrl = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);

        lv = v.findViewById(R.id.allergy_resultListView);
        addAllergy = v.findViewById(R.id.allergy_addBtn);

        int value = -1; // or other values
        value = getArguments().getInt("id");


        addAllergy.setOnClickListener((View w) -> {
            Intent i = new Intent(getContext(), AddAllergyActivity.class);
            Bundle idDmpB = new Bundle();
            idDmpB.putInt("id",getArguments().getInt("id"));
            i.putExtras(idDmpB);
            startActivityForResult(i, 1);

        });

        return v;
    }


    private void sendGetLoadRequest(JSONObject request, String url) {
        CustomJsonObjectRequestArrayResponse jsonObjectRequest = new CustomJsonObjectRequestArrayResponse
                (Request.Method.GET, url, request, token, this::fillListView, error -> {
                    ToasterMaker.makeToast(getContext(), "Search Error");
                    Log.e(LOG_TAG, error.toString());
                });
        HttpSender.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }


    private void fillListView(JSONArray response) {
        ArrayList<HashMap<String, String>> allergies = new ArrayList<>();
        HashMap<String, String> item;
        for (int i = 0; i < response.length(); i++) {
            // Get current json object
            try {
                JSONObject allergyJson = response.getJSONObject(i);
                item = new HashMap<String, String>();
                item.put("an", allergyJson.getString("allergyName"));
                item.put("dc", allergyJson.getString("dateCreation"));
                allergies.add(item);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        SimpleAdapter adapter;
        adapter = new SimpleAdapter(getContext(), allergies,
                R.layout.multi_result_allergy,
                new String[]{"an", "dc"},
                new int[]{R.id.multiResultAllergy_line_a, R.id.multiResultAllergy_line_b});
        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
