package fr.asclepios.sirhp.acte;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.document.DocumentActivity;
import fr.asclepios.sirhp.entity.Acte;
import fr.asclepios.sirhp.request.CustomBooleanRequest;
import fr.asclepios.sirhp.request.HttpSender;
import fr.asclepios.sirhp.utils.ToasterMaker;

public class ShowActeActivity extends AppCompatActivity {
    public static final String ID_ACTE = "fr.asclepios.sirhp.extra.ID_ACTE";
    private Acte acte;

    private String token;
    private String urlServer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        token = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE).getString("token", "");
        urlServer = getSharedPreferences("GLOBALS", MODE_PRIVATE).getString("url", null);
        setContentView(R.layout.activity_showacte);
        Button fileDisplay = (Button) findViewById(R.id.acte_go_to_files);
        Button validateActe = (Button) findViewById(R.id.acte_validate);
        TextView txtTypeActeAndDate = (TextView) findViewById(R.id.acte_typeActe_and_date);

        Bundle bundle = getIntent().getExtras();
        acte = new Acte(bundle.getInt("idActe"), bundle.getBoolean("validationActe"));
        txtTypeActeAndDate.setText(bundle.getString("typeActeAndDate"));

        if(acte.isValidationActe()){
            validateActe.setVisibility(View.INVISIBLE);
        }
    }

    private void valideActe(){
        JSONObject request = new JSONObject();
        int idPHValid = 1;

        String url = urlServer+"acte/"+acte.getIdActe()+"/PH/"+idPHValid;
        sendPostRequest(request, url);
    }

    private void sendPostRequest(JSONObject request, String url) {
        CustomBooleanRequest jsonObjectRequest = new CustomBooleanRequest
                (Request.Method.POST, url, request, token, this::valideActeResult, error -> {
                    ToasterMaker.makeToast(this, "Connection Error:" + error );

                    Log.e("ERROR","" + error);

                });

        // Access the RequestQueue through your singleton class.
        HttpSender.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }
    private void valideActeResult(boolean response){
        if(!response){
            ToasterMaker.makeToast(this, "Acte non validé");
        }
        else{
            ToasterMaker.makeToast(this, "Acte validé");
            Button validateActe = (Button) findViewById(R.id.acte_validate);
            validateActe.setVisibility(View.INVISIBLE);
        }
    }

    public void validateBtnHandler(View view)//TODO :clarify
    {
        int idphConnnected = 1;
        int idDMP = 1;
        int idActe = 1;
        SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String s = parseFormat.format(date);
        valideActe();
    }

    public void documentsBtnHandler(View view)
    {
        Intent intent = new Intent(this, DocumentActivity.class);
        intent.putExtra(ID_ACTE, acte.getIdActe());
        startActivity(intent);
    }


}
