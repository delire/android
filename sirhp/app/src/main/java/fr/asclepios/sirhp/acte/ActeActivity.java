package fr.asclepios.sirhp.acte;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import fr.asclepios.sirhp.R;

public class ActeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acte);
        Button addActe = (Button) findViewById(R.id.acte_button_add);
        Button searchActe = (Button) findViewById(R.id.acte_button_search);

        addActe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ActeActivity.this, AddActeActivity.class);
                startActivity(myIntent);
            }
        });

        searchActe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ActeActivity.this, SearchActeFragment.class);
                startActivity(myIntent);
            }
        });

    }
}
