package fr.asclepios.sirhp;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class MainFragment extends Fragment {

    public MainFragment() {
        // Required empty public constructor
    }

    private String token;
    private String baseUrl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View v = inflater.inflate(R.layout.fragmen_main_layout, container, false);
        token = getContext().getApplicationContext().getSharedPreferences("UserDetails", getContext().MODE_PRIVATE).getString("token", "");
        baseUrl = getContext().getSharedPreferences("GLOBALS", getContext().MODE_PRIVATE).getString("url", null);


        return v;
    }

}
