package fr.asclepios.sirhp.location;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.Circle;
import com.mapbox.mapboxsdk.plugins.annotation.CircleManager;
import com.mapbox.mapboxsdk.plugins.annotation.CircleOptions;
import com.mapbox.mapboxsdk.utils.ColorUtils;

import java.util.ArrayList;
import java.util.Arrays;

import fr.asclepios.sirhp.R;
import fr.asclepios.sirhp.request.CustomJsonArrayRequest;
import fr.asclepios.sirhp.request.HttpSender;


public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback, ServiceConnection, LocationObserver {
    private static final String LOG_TAG = LocationActivity.class.getSimpleName();
    private static final long LOAD_INTERVAL_IN_MILLISECONDS = 10000L;// 1 min//TODO modify

    private String url;
    private String token;
    private Gson gson;
    private Handler handler;
    private Runnable loadTask;
    private boolean loadTaskOn;
    private boolean bound;


    private MapboxMap mapboxMap;
    private MapView mapView;
    private CircleManager circleManager;
    private Circle circle;
    private LocationService locationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String baseUrl = getSharedPreferences("GLOBALS", MODE_PRIVATE).getString("url", null);
        url = baseUrl + LocationService.URL_PATH;
        token = getApplicationContext().getSharedPreferences("UserDetails", MODE_PRIVATE).getString("token", "");
        gson = new Gson();
        loadTaskOn = false;
        bound = false;
        handler = new Handler();
        loadTask = new Runnable() {
            @Override
            public void run() {
                load();
                loadTaskOn = handler.postDelayed(this, LOAD_INTERVAL_IN_MILLISECONDS);
            }
        };

        Mapbox.getInstance(this, getString(R.string.access_token));

        setContentView(R.layout.activity_location);

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
            circleManager = new CircleManager(mapView, mapboxMap, style);
            Intent intent = new Intent(this, LocationService.class);
            bindService(intent, this, Context.BIND_AUTO_CREATE);
            startLoadTask();
        });
    }


    private void load() {
        Log.d(LOG_TAG, "loading locations");
        CustomJsonArrayRequest locationsRequest = new CustomJsonArrayRequest(url, null, token, response -> {
            ArrayList<fr.asclepios.sirhp.entity.Location> locations = new ArrayList<>(Arrays.asList(gson.fromJson(response.toString(), fr.asclepios.sirhp.entity.Location[].class)));
            mapboxMap.clear();
            for (fr.asclepios.sirhp.entity.Location location : locations) {
                StringBuilder sb = new StringBuilder();
                for (String role : location.getRoles()) sb.append(role + " ");
                mapboxMap.addMarker(new MarkerOptions().setTitle(location.getFname() + " " + location.getLname()).setSnippet(sb.toString()).position(new LatLng(location.getLatitude(), location.getLongitude())));
            }
        }, error -> {
            Log.e(LOG_TAG, error.toString());
        });
        HttpSender.getInstance(this).addToRequestQueue(locationsRequest);
    }

    private void startLoadTask() {
        if (!loadTaskOn) {
            loadTaskOn = handler.postDelayed(loadTask, LOAD_INTERVAL_IN_MILLISECONDS);
        }
    }

    private void stopLoadTask() {
        handler.removeCallbacks(loadTask);
        loadTaskOn = false;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        LocationService.MyBinder binder = (LocationService.MyBinder) service;
        locationService = binder.getService();
        locationService.subscribe(this);
        bound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        bound = false;
    }

    @Override
    public void notify(Location location) {
        Log.d(LOG_TAG, "location update");
        if (circle != null)
            circleManager.delete(circle);
        CircleOptions circleOptions = new CircleOptions()
                .withLatLng(new LatLng(location.getLatitude(), location.getLongitude()))
                .withCircleColor(ColorUtils.colorToRgbaString(Color.RED));
        circle = circleManager.create(circleOptions);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        stopLoadTask();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        stopLoadTask();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if (bound)
            locationService.free(this);
        unbindService(this);
        stopLoadTask();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}