package fr.asclepios.sirhp.request;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

public class FileRequest extends VolleyMultipartRequest {
    private String token;
    private DataPart dataPart;

    public FileRequest(DataPart dataPart, String url, String token, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
        super(Request.Method.POST, url, listener, errorListener);
        this.token = token;
        this.dataPart = dataPart;
    }

    @Override
    protected Map<String, DataPart> getByteData() {
        Map<String, DataPart> params = new HashMap<>();
        params.put("file", this.dataPart);
        return params;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + token);
        return headers;
    }
}
